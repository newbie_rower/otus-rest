using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly CustomerRepository _customerRepository;

        public CustomerController(CustomerRepository CustomerRepository)
        {
            _customerRepository = CustomerRepository;
        }

        [HttpGet("{id:long}")]
        public string GetCustomer([FromRoute] long id)
        {
            return _customerRepository.GetById(id).ToString();
        }

        [HttpPost("")]
        public Task<HttpStatusCode> CreateCustomerAsync([FromBody] Customer customer)
        {
            return _customerRepository.AddCustomer(customer);
        }
    }
}