﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;
using System.Net;
using System.Net.Http;

namespace WebApi.Services
{
    public class CustomerRepository
    {
        public List<Customer> CustomerRepo;

        public CustomerRepository()
        {
            CustomerRepo = new List<Customer>
                    {
                        new Customer {Id = 1, Firstname = "First", Lastname = "First"},
                        new Customer {Id = 2, Firstname = "Second", Lastname ="Second"},
                        new Customer {Id = 3, Firstname = "Third", Lastname = "Third",},
                        new Customer {Id = 4, Firstname = "Fourth", Lastname ="Fourth"},
                        new Customer {Id = 5, Firstname = "Fifth", Lastname = "Fifth"},
                    };
        }

        public string GetById(long id)
        {
            var customer = CustomerRepo.Find(x => x.Id == id);
            if (customer != null)
            {
                return $"ID: {customer.Id} Firstname: {customer.Firstname} Lastname: {customer.Lastname}";
            }

            return HttpStatusCode.NotFound.ToString();
        }

        public Task<HttpStatusCode> AddCustomer(Customer customer)
        {
            if (CustomerRepo.Find(x => x.Id == customer.Id) != null)
            {
                return Task.FromResult(HttpStatusCode.Conflict);
            }

            CustomerRepo.Add(customer);
            return Task.FromResult(HttpStatusCode.OK);
        }
    }
}
