﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        const string url = "https://localhost:5001/customers/";
        public static void Main(string[] args)
        {
            var testCustomer = new Customer
            {
                Id = new Random().Next(1, int.MaxValue),
                Firstname = "FirstName" + new Random().Next(int.MinValue, int.MaxValue).ToString(),
                Lastname = "LastName" + new Random().Next(int.MinValue, int.MaxValue).ToString()
            };

            _ = PostCustomer(url, testCustomer);
            _ = PostCustomer(url, testCustomer);

            long inputID = InputId();
            _ = GetCustomer(url, inputID);

            Console.WriteLine("----------------");

            Console.WriteLine($"Test customer ID: {testCustomer.Id}");
            _ = GetCustomer(url, testCustomer.Id);

            Console.ReadKey();

        }
        private static long InputId()
        {
            Console.WriteLine("Ввод ID пользователя");
            if (!long.TryParse(Console.ReadLine(), out long id) && id < 0)
            {
                return 0;
            }
            return id;
        }
        private static async Task GetCustomer(string url, long id)
        {
            using var client = new HttpClient();

            var response = await client.GetAsync(url + id.ToString());

            Console.WriteLine(response.Content.ReadAsStringAsync().Result);
        }

        private static async Task PostCustomer(string url, Customer customer)
        {

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(customer);
            var data = new System.Net.Http.StringContent(json, Encoding.UTF8, "application/json");

            using var client = new HttpClient();

            var response = await client.PostAsync(url, data);

            string result = response.Content.ReadAsStringAsync().Result;
            Console.WriteLine(result);
        }
    }
}